<?php
require_once('fpdf/html2pdf.php');
require_once('config.php');

$csv_datei = "CSV/Kunden.csv";
$fedler_trenner = ";";
$zeilen_trenner = "\n";

if (@file_exists($csv_datei) == false) {
	echo 'Die CSV Datei: '. $csv_datei.' gibt es nicht!';
} 
else{
	$datei_inhalt = @file_get_contents($csv_datei);
	$zeilen = explode($zeilen_trenner,$datei_inhalt);
	
	if (is_array($zeilen) == true) {
		
		foreach($zeilen as $zeile) {
			list($salutation,$forename,$surname,$email) = explode($fedler_trenner,$zeile);
			
			// email stuff (change data below)
			$to = $email;
			$data = "Email.htm";
			if (@file_exists($data) == false) {
				echo 'Die Datei: '. $data.' gibt es nicht!';
			}
			else {
				$htmlDatei = @file_get_contents($data);							
				$htmlDatei = str_replace("@SALUTATION@",$salutation,$htmlDatei);
				$htmlDatei = str_replace("@FORENAME@",$forename,$htmlDatei);
				$htmlDatei = str_replace("@SURNAME@",$surname,$htmlDatei);
				
				$userPath = "users/".$surname.$forename;
				if (is_dir($userPath)) {
					echo "Ordner existiert";
				} 
				else {
					mkdir ($userPath,0755);
				}
				
				$filename = "Email_".$forename.".pdf";
				$file = $userPath."/".$filename;
				
				$pdf=new PDF_HTML();
				$pdf->AddPage();
				$pdf->SetFont('Times');
				$pdf->WriteHTML($htmlDatei);
				
				if(!file_exists($file)){
					$pdf->Output($file,"F");
				}
				else{
					$pdf->Output("","S");
				}
				
				$headers = "From: $fromName"." <".$from.">";
				
				//boundary 
				$semi_rand = md5(time()); 
				$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 

				//headers for attachment 
				$headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\""; 

				//multipart boundary 
				$message = "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"iso-8859-1\"\n" .
				"Content-Transfer-Encoding: 7bit\n\n" . $htmlDatei . "\n\n"; 

				//preparing attachment
				if(!empty($file) > 0){
					if(is_file($file)){
						$message .= "--{$mime_boundary}\n";
						$fp =    @fopen($file,"rb");
						$data =  @fread($fp,filesize($file));

						@fclose($fp);
						$data = chunk_split(base64_encode($data));
						$message .= "Content-Type: application/odf; name=\"".basename($file)."\"\n" . 
						"Content-Description: ".basename($file)."\n" .
						"Content-Disposition: attachment;\n" . " filename=\"".basename($file)."\"; size=".filesize($file).";\n" . 
						"Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
					}
				}
				$message .= "--{$mime_boundary}--";
				
				//send email
				$mail = @mail($to, $subject, $message, $headers); 

				//email sending status
				echo $mail?"<h1>Mail sent.</h1>":"<h1>Mail sending failed.</h1>";
			}
		}
	}
}
?>